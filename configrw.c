/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is AOLserver Code and related documentation
 * distributed by AOL.
 * 
 * The Initial Developer of the Original Code is America Online,
 * Inc. Portions created by AOL are Copyright (C) 1999 America Online,
 * Inc. All Rights Reserved.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU General Public License (the "GPL"), in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above, a recipient may use your
 * version of this file under either the License or the GPL.
 */

/*
 * nsconflib.c --
 *
 *      Library for access to and manipulation of the configuration storage.
 */

#include "nsconfigrw.h"

NS_RCSID("@(#) $Header$");


/*
 * Sections appear once in the global table, and once in each
 * thread table for those threads which accesses a parameter
 * in the global section.
 */

typedef struct Section {

    char           *name;       /* Name of the section */

    Ns_Mutex        lock;       /* Lock around params and section list. */
    Tcl_HashTable   params;     /* Parameters and values */

    /*
     * The following maintains a list of thread-sections
     * to update when a global parameter is set.
     */

    struct Section *nextPtr;
    struct Section *prevPtr;

} Section;

/*
 * The following structure represents one configuration 
 * parameter as stored in Section->params table.
 * This can be extended to add more key attributes like
 * scope, mode etc...
 */

typedef enum {
    UnknownType,
    BoolType,
    IntType
} ParamType;

typedef struct Param {
    ParamType type;         /* One of the supported types */
    int       intval;       /* The integer values */
    int       size;         /* Size of the string value */
    char     *value;        /* The string value */
} Param;

#define ClearParam(a)                          \
    (a)->value = NULL;                         \
    (a)->type  = UnknownType                   \


/*
 * Static functions defined in this file.
 */

static int  CfgGet(CONST char *section, CONST char *key, Param *def, Param *par)
    NS_GNUC_NONNULL(1) NS_GNUC_NONNULL(2);
static void CfgSet(CONST char *section, CONST char *key, CONST Param *param)
    NS_GNUC_NONNULL(1) NS_GNUC_NONNULL(2) NS_GNUC_NONNULL(3);

static Section *GetSection(Tcl_HashTable *sectionTable, CONST char *section);

static Param *GetParam(Section *section, CONST char *key);
static void   SetParam(Section *section, CONST char *key, CONST Param *param);
static void   CopyParam(Param *target, CONST Param *source);
static void   FreeParam(Param *param);

static int    GetStringFromParam(Param *param, char **value, int *size);
static int    GetIntFromParam(Param *param, int *value);
static int    GetBoolFromParam(Param *param, int *value);

static Ns_Callback ThreadCleanup;

/*
 * Static variables defined in this file.
 */

static Tcl_HashTable globalTable;  /* Global table of sections. */
static Ns_Mutex      lock;         /* Lock around global sections. */

static Ns_Tls        tls;          /* Key for per-thread config. */



/*
 *----------------------------------------------------------------------
 *
 * Cfg_LibInit --
 *
 *      Library initialisation.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
Cfg_LibInit(void)
{
    static int once = 0;

    Ns_MutexLock(&lock);
    if (!once) {
        once = 1;
        Ns_MutexSetName(&lock, "ns:conf");
        Tcl_InitHashTable(&globalTable, TCL_STRING_KEYS);
        Ns_TlsAlloc(&tls, ThreadCleanup);
    }
    Ns_MutexUnlock(&lock);
}


/*
 *----------------------------------------------------------------------
 *
 * Cfg_GetString --
 *
 *      Returns a string from the configuration.
 *
 * Results:
 *      NS_TRUE if found, NS_FALSE if not, NS_ERROR on conversion error
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

int
Cfg_GetString(CONST char *section, CONST char *key, CONST char *def,
              Tcl_DString *dsPtr)
{
    int    status, size;
    char  *result;
    Param  outpar, defpar, *defPtr = NULL;

    ClearParam(&outpar);

    if (def != NULL) {
        defpar.type  = UnknownType;
        defpar.value = (char *) def;
        defpar.size  = strlen(def);
        defPtr = &defpar;
    }

    status = CfgGet(section, key, defPtr, &outpar);
    if (status == NS_TRUE) {
        status = GetStringFromParam(&outpar, &result, &size);
        if (status == NS_OK) {
            status = NS_TRUE;
            Tcl_DStringAppend(dsPtr, result, size);
            Ns_Log(Debug, "configrw: %s:%s value=\"%s\" default=\"%s\" (string)",
                   section, key, result, def ? def : "(null)");
        }
        FreeParam(&outpar);
    }

    return status;
}


/*
 *----------------------------------------------------------------------
 *
 * Cfg_GetBool --
 *
 *      Retrieves a boolean from the configuration.
 *
 * Results:
 *      NS_TRUE if found, NS_FALSE if not, NS_ERROR on conversion error
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

int
Cfg_GetBool(CONST char *section, CONST char *key, CONST int *def, int *value)
{
    Param outpar, defpar, *defPtr = NULL;
    int   result, status;

    ClearParam(&outpar);

    if (def != NULL) {
        defpar.type   = BoolType;
        defpar.intval = (*def != 0);
        defPtr = &defpar;
    }

    status = CfgGet(section, key, defPtr, &outpar);
    if (status == NS_TRUE) {
        status = GetBoolFromParam(&outpar, &result);
        if (status == NS_OK) {
            status = NS_TRUE;
            *value = (result != 0);
            Ns_Log(Debug, "configrw: %s:%s value=%s default=%s (bool)", 
                   section, key, value ? "true" : "false", 
                   def ? (*def ? "true" : "false") : "(none)");
        }
        FreeParam(&outpar);
    }

    return status;
}


/*
 *----------------------------------------------------------------------
 *
 * Cfg_GetInt --
 *
 *      Retrieves a integer from the configuration.
 *
 * Results:
 *      NS_TRUE if found, NS_FALSE if not, NS_ERROR on conversion error
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

int 
Cfg_GetInt(CONST char *section, CONST char *key, CONST int *def, int *value)
{
    return Cfg_GetRange(section, key, def, INT_MIN, INT_MAX, value);
}


/*
 *----------------------------------------------------------------------
 *
 * Cfg_GetRange --
 *
 *      Retrieves integer parameter from the configuration
 *      imposing user given limits. The limits are imposed
 *      on the returned value only.
 *
 * Results:
 *      NS_TRUE if found, NS_FALSE if not, NS_ERROR on conversion error
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

int
Cfg_GetRange(CONST char *section, CONST char *key, CONST int *def, 
             CONST int min, CONST int max, int *value)
{
    Param outpar, defpar, *defPtr = NULL;
    int   result, status;

    ClearParam(&outpar);

    if (def != NULL) {
        defpar.type   = IntType;
        defpar.intval = *def;
        defPtr = &defpar;
    }

    status = CfgGet(section, key, defPtr, &outpar);
    if (status == NS_TRUE) {
        status = GetIntFromParam(&outpar, &result);
        if (status == NS_OK) {
            status = NS_TRUE;
            *value = result;
            if (def != NULL) {
                Ns_Log(Debug, "configrw: %s:%s value=%d "
                       "min=%d max=%d default=%d (int)",
                       section, key, result, min, max, *def);
            } else {
                Ns_Log(Debug, "configrw: %s:%s value=%d"
                       "min=%d max=%d default=%s (int)",
                       section, key, result, min, max, "(none)");
            }
        }
        FreeParam(&outpar);
    }
    if (status == NS_TRUE) {
        if (*value < min) {
            Ns_Log(Warning, "configrw: %s:%s value=%d, rounded up to %d",
                   section, key, *value, min);
            *value = min;
        }
        if (*value > max) {
            Ns_Log(Warning, "configrw: %s:%s value=%d, rounded down to %d",
                   section, key, *value, max);
            *value = max;
        }
    }

    return status;
}


/*
 *----------------------------------------------------------------------
 *
 * Cfg_SetString --
 *
 *      Sets a string parameter in the configuration.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
Cfg_SetString(CONST char *section, CONST char *key, char *value)
{
    Param param;

    param.type  = UnknownType;
    param.value = value;
    param.size  = strlen(param.value);
    CfgSet(section, key, &param);
}


/*
 *----------------------------------------------------------------------
 *
 * Cfg_SetInt --
 *
 *      Sets a integer parameter in the configuration.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
Cfg_SetInt(CONST char *section, CONST char *key, int value)
{
    Param param;

    param.type   = IntType;
    param.intval = value;
    CfgSet(section, key, &param);
}


/*
 *----------------------------------------------------------------------
 *
 * Cfg_SetBool --
 *
 *      Sets a boolean parameter in the configuration.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

void
Cfg_SetBool(CONST char *section, CONST char *key, int value)
{
    Param param;

    param.type   = BoolType;
    param.intval = (value != 0);
    CfgSet(section, key, &param);
}


/*
 *----------------------------------------------------------------------
 *
 * CfgGetObjCmd --
 *
 *      Implements: ns_getconfig
 *
 * Results:
 *      Standard Tcl result.
 *
 * Side effects:
 *      TBD.
 *
 *----------------------------------------------------------------------
 */

int
CfgGetObjCmd(ClientData data, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
    Section        *globalSection;
    Tcl_HashSearch  search;
    Tcl_HashEntry  *hPtr;
    Ns_DString      ds;
    Tcl_Obj        *defObj = NULL;
    char           *def, *section = NULL, *key = NULL;
    int             i, isbool = 0, isint = 0, status;
    int             min = INT_MIN, max = INT_MAX;

    Ns_ObjvSpec opts[] = {
        {"-bool",  Ns_ObjvBool,  &isbool, (void *) NS_TRUE},
        {"-int",   Ns_ObjvBool,  &isint,  (void *) NS_TRUE},
        {"-min",   Ns_ObjvInt,   &min,    NULL},
        {"-max",   Ns_ObjvInt,   &max,    NULL},
        {"--",     Ns_ObjvBreak, NULL,    NULL},
        {NULL, NULL, NULL, NULL}
    };
    Ns_ObjvSpec args[] = {
        {"?section", Ns_ObjvString, &section, NULL},
        {"?key",     Ns_ObjvString, &key,     NULL},
        {"?default", Ns_ObjvObj,    &defObj,  NULL},
        {NULL, NULL, NULL, NULL}
    };
    if (Ns_ParseObjv(opts, args, interp, 1, objc, objv) != NS_OK) {
        return TCL_ERROR;
    }
    if (min > INT_MIN || max < INT_MAX) {
        isint = 1;
    }

    /*
     * List all sections.
     */

    if (section == NULL) {
        Ns_MutexLock(&lock);
        hPtr = Tcl_FirstHashEntry(&globalTable, &search);
        while (hPtr != NULL) {
            section = (char *) Tcl_GetHashKey(&globalTable, hPtr);
            Tcl_AppendElement(interp, section);
            hPtr = Tcl_NextHashEntry(&search);
        }
        Ns_MutexUnlock(&lock);

        return TCL_OK;
    }

    /*
     * List all keys in a section.
     */

    if (key == NULL) {
        Ns_MutexLock(&lock);
        globalSection = GetSection(&globalTable, section);
        hPtr = Tcl_FirstHashEntry(&globalSection->params, &search);
        while (hPtr != NULL) {
            key = (char *) Tcl_GetHashKey(&globalSection->params, hPtr);
            Tcl_AppendElement(interp, key);
            hPtr = Tcl_NextHashEntry(&search);
        }
        Ns_MutexUnlock(&lock);

        return TCL_OK;
    }

    /*
     * Lookup the key in it's section.
     */

    i = 0; /* Force a default for bool and int... ? */

    if (isbool) {
        if (defObj != NULL
            && Tcl_GetBooleanFromObj(interp, defObj, &i) != TCL_OK) {
            return TCL_ERROR;
        }
        status = Cfg_GetBool(section, key, defObj ? &i : NULL, &i);
        switch (status) {
        case NS_ERROR:
            Tcl_SetResult(interp, "configuration parameter is not a boolean",
                          TCL_STATIC);
            return TCL_ERROR;
        case NS_FALSE:
            return TCL_OK;
        }
        Tcl_SetObjResult(interp, Tcl_NewBooleanObj(i));

    } else if (isint) {
        if (defObj != NULL
            && Tcl_GetIntFromObj(interp, defObj, &i) != TCL_OK) {
            return TCL_ERROR;
        }
        if ((min > INT_MIN || max < INT_MAX)) {
            status = Cfg_GetRange(section, key, defObj ? &i : NULL, min, max, &i);
        } else {
            status = Cfg_GetInt(section, key, defObj ? &i : NULL, &i);
        }
        switch (status) {
        case NS_ERROR:
            Tcl_SetResult(interp, "configuration parameter is not an integer",
                          TCL_STATIC);
            return TCL_ERROR;
        case NS_FALSE:
            return TCL_OK;
        }
        Tcl_SetObjResult(interp, Tcl_NewIntObj(i));

    } else {
        def = (defObj != NULL) ? Tcl_GetString(defObj) : NULL;
        Tcl_DStringInit(&ds);
        status = Cfg_GetString(section, key, defObj ? def : NULL, &ds);
        switch (status) {
        case NS_ERROR:
            Tcl_SetResult(interp, "configuration parameter is invalid",
                          TCL_STATIC);
            return TCL_ERROR;
        case NS_FALSE:
            return TCL_OK;
        }
        Tcl_DStringResult(interp, &ds);
    }

    return TCL_OK;
}


/*
 *----------------------------------------------------------------------
 *
 * CfgSetObjCmd --
 *
 *      Implements: ns_setconfig
 *
 * Results:
 *      Standard Tcl result.
 *
 * Side effects:
 *      TBD.
 *
 *----------------------------------------------------------------------
 */

int
CfgSetObjCmd(ClientData data, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
    Tcl_Obj  *valObj;
    char     *section, *key;
    int       i, isbool = 0, isint = 0;

    Ns_ObjvSpec opts[] = {
        {"-bool",  Ns_ObjvBool,  &isbool, (void *) NS_TRUE},
        {"-int",   Ns_ObjvBool,  &isint,  (void *) NS_TRUE},
        {"--",     Ns_ObjvBreak, NULL,    NULL},
        {NULL, NULL, NULL, NULL}
    };
    Ns_ObjvSpec args[] = {
        {"section", Ns_ObjvString, &section, NULL},
        {"key",     Ns_ObjvString, &key,     NULL},
        {"value",   Ns_ObjvObj,    &valObj,  NULL},
        {NULL, NULL, NULL, NULL}
    };
    if (Ns_ParseObjv(opts, args, interp, 1, objc, objv) != NS_OK) {
        return TCL_ERROR;
    }

    if (isbool) {
        if (Tcl_GetBooleanFromObj(interp, valObj, &i) != TCL_OK) {
            return TCL_ERROR;
        }
        Cfg_SetBool(section, key, i);

    } else if (isint) {
        if (Tcl_GetIntFromObj(interp, valObj, &i) != TCL_OK) {
            return TCL_ERROR;
        }
        Cfg_SetInt(section, key, i);

    } else {
        Cfg_SetString(section, key, Tcl_GetString(valObj));
    }

    return TCL_OK;
}


/*
 *----------------------------------------------------------------------
 *
 * CfgGet --
 *
 *      Get a parameter, looking first in the thread cache, then the
 *      global section.
 *
 * Results:
 *      NS_TRUE if found, NS_FALSE otherwise.
 *
 * Side effects:
 *      Creates the per-thread table of sections on first access.
 *
 *----------------------------------------------------------------------
 */

static int
CfgGet(CONST char *section, CONST char *key, Param *defPtr, Param *outPtr)
{
    Tcl_HashTable *threadTable;
    Section       *threadSection, *globalSection;
    Param         *parPtr;

    threadTable = Ns_TlsGet(&tls);
    if (threadTable == NULL) {
        threadTable = ns_malloc(sizeof *threadTable);
        Tcl_InitHashTable(threadTable, TCL_STRING_KEYS);
        Ns_TlsSet(&tls, threadTable);
    }
    threadSection = GetSection(threadTable, section);

    Ns_MutexLock(&threadSection->lock);
    parPtr = GetParam(threadSection, key);
    if (parPtr != NULL) {
        CopyParam(outPtr, parPtr);
    }
    Ns_MutexUnlock(&threadSection->lock);

    if (parPtr == NULL) {
        Ns_MutexLock(&lock);
        globalSection = GetSection(&globalTable, section);
        parPtr = GetParam(globalSection, key);
        if (parPtr == NULL && defPtr != NULL) {
            parPtr = defPtr;
            SetParam(globalSection, key, parPtr);
        }
        if (parPtr != NULL) {
            CopyParam(outPtr, parPtr);
            if (threadSection->prevPtr == NULL) {
                threadSection->prevPtr = globalSection;
                threadSection->nextPtr = globalSection->nextPtr;
                globalSection->nextPtr = threadSection;
            }

            /*
             * Update thread section with the global value
             * to avoid hitting the global section again on
             * repeating request for the same parameter.
             *
             * No need to hold thread-section lock here. 
             * No other writer can enter this part as we
             * are holding the global mutex already. 
             */

            SetParam(threadSection, key, parPtr);
        }
        Ns_MutexUnlock(&lock);
    }

    return (parPtr != NULL) ? NS_TRUE : NS_FALSE;
}


/*
 *----------------------------------------------------------------------
 *
 * CfgSet --
 *
 *      Set the value of a parameter in the global section.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Updates threads that have previously accessed this parameter
 *      with the new value.
 *
 *----------------------------------------------------------------------
 */

static void
CfgSet(CONST char *section, CONST char *key, CONST Param *param)
{
    Section  *globalSection, *threadSection;

    Ns_MutexLock(&lock);
    globalSection = GetSection(&globalTable, section);
    SetParam(globalSection, key, param);

    threadSection = globalSection->nextPtr;
    while (threadSection != NULL) {
        Ns_MutexLock(&threadSection->lock);
        SetParam(threadSection, key, param);
        Ns_MutexUnlock(&threadSection->lock);
        threadSection = threadSection->nextPtr;
    }
    Ns_MutexUnlock(&lock);
}


/*
 *----------------------------------------------------------------------
 *
 * GetSection --
 *
 *      Return a pointer to the requested section. Locking is the
 *      callers responsibility.
 *
 * Results:
 *      Section pointer.
 *
 * Side effects:
 *      Allocates and initializes the section on first accesss.
 *
 *----------------------------------------------------------------------
 */

static Section *
GetSection(Tcl_HashTable *sectionTable, CONST char *section)
{
    Section       *secPtr;
    Tcl_HashEntry *hPtr;
    int            new;

    hPtr = Tcl_CreateHashEntry(sectionTable, section, &new);
    if (!new) {
        secPtr = (Section *) Tcl_GetHashValue(hPtr);
    } else {
        secPtr = ns_malloc(sizeof(Section));
        secPtr->name = (char *) Tcl_GetHashKey(sectionTable, hPtr);
        Ns_MutexInit(&secPtr->lock);
        Ns_MutexSetName2(&secPtr->lock, "ns:cfg", section);
        Tcl_InitHashTable(&secPtr->params, TCL_STRING_KEYS);
        secPtr->nextPtr = secPtr->prevPtr = NULL;
        Tcl_SetHashValue(hPtr, secPtr);
    }

    return secPtr;
}


/*
 *----------------------------------------------------------------------
 *
 * GetParam --
 *
 *      Lookup the key in the given section and return the parameter
 *      structure corresponding to that key.
 *
 * Results:
 *      Pointer ot parameter structure or NULL if nothing found.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

static Param *
GetParam(Section *secPtr, CONST char *name)
{
    Param         *parPtr = NULL;
    Tcl_HashEntry *hPtr;

    hPtr = Tcl_FindHashEntry(&secPtr->params, name);
    if (hPtr != NULL) {
        parPtr = (Param *) Tcl_GetHashValue(hPtr);
    }

    return parPtr;
}

/*
 *----------------------------------------------------------------------
 *
 * SetParam --
 *
 *      Set the parameter for the given section and key.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Allocates and initializes the paramter if the parameter 
 *      was not found.
 *
 *----------------------------------------------------------------------
 */

static void
SetParam(Section *secPtr, CONST char *name, CONST Param *param)
{
    Param         *parPtr = NULL;
    Tcl_HashEntry *hPtr;
    int            new;

    hPtr = Tcl_CreateHashEntry(&secPtr->params, name, &new);

    if (!new) {
        parPtr = (Param *) Tcl_GetHashValue(hPtr);
        FreeParam(parPtr);
        CopyParam(parPtr, param);
    } else {
        parPtr = ns_calloc(1, sizeof(Param));
        CopyParam(parPtr, param);
        Tcl_SetHashValue(hPtr, parPtr);
    }
}

/*
 *----------------------------------------------------------------------
 *
 * GetStringFromParam --
 *
 *      Get the string representation from the parameter.
 *
 * Results:
 *      NS_OK if conversion suceeded, NS_ERROR otherwise.
 *
 * Side effects:
 *      May convert the parameter to string type.
 *
 *----------------------------------------------------------------------
 */

static int
GetStringFromParam(Param *param, char **value, int *size)
{
    if (param->type != UnknownType) {
        char buf[TCL_INTEGER_SPACE];
        int nbytes;

        nbytes = sprintf(buf, "%d", param->intval);
        if (nbytes < 0) {
            return NS_ERROR;
        }
        if (param->value != NULL) {
            ns_free(param->value);
        }
        param->value = ns_malloc(nbytes + 1);
        memcpy(param->value, buf, nbytes);
        *(param->value + nbytes) = 0;
        param->size = nbytes;
        param->type = UnknownType;
    }

    *size  = param->size;
    *value = param->value;

    return NS_OK;
}


/*
 *----------------------------------------------------------------------
 *
 * GetBoolFromParam --
 *
 *      Get the boolean representation from the parameter.
 *
 * Results:
 *      NS_OK if conversion suceeded, NS_ERROR otherwise.
 *
 * Side effects:
 *      May convert the parameter to int type.
 *
 *----------------------------------------------------------------------
 */

static int
GetBoolFromParam(Param *param, int *result)
{
    if (param->type == UnknownType) {
        if (Tcl_GetBoolean(NULL, param->value, &param->intval) != TCL_OK) {
            return NS_ERROR;
        }
        param->type = BoolType;
    } else if (param->type != BoolType) {
        return NS_ERROR;
    }
    
    *result = param->intval;

    return NS_OK;
}


/*
 *----------------------------------------------------------------------
 *
 * GetIntFromParam --
 *
 *      Get the integer representation from the parameter.
 *
 * Results:
 *      NS_OK if conversion suceeded, NS_ERROR otherwise.
 *
 * Side effects:
 *      May convert the parameter to int type.
 *
 *----------------------------------------------------------------------
 */

static int
GetIntFromParam(Param *param, int *result)
{
    if (param->type == UnknownType) {
        if (Tcl_GetInt(NULL, param->value, &param->intval) != TCL_OK) {
            return NS_ERROR;
        }
        param->type = IntType;
    } else if (param->type != IntType) {
        return NS_ERROR;
    }

    *result = param->intval;

    return NS_OK;
}


/*
 *----------------------------------------------------------------------
 *
 * CopyParam --
 *
 *      Copies parameter values. Freeing old parameter is the callers
 *      responsibility.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

static void
CopyParam(Param *dest, CONST Param *src)
{
    dest->type = src->type;

    if (src->type == IntType || src->type == BoolType) {
        dest->intval = src->intval;
    } else {
        dest->size  = src->size;
        dest->value = ns_malloc(dest->size + 1);
        memcpy(dest->value, src->value, dest->size);
        *(dest->value + dest->size) = 0;
    }
}

/*
 *----------------------------------------------------------------------
 *
 * FreeParam --
 *
 *      Frees parameter values.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      None.
 *
 *----------------------------------------------------------------------
 */

static void
FreeParam(Param *param)
{
    if (param->value != NULL) {
        ns_free(param->value);
    }
    ClearParam(param);
}


/*
 *----------------------------------------------------------------------
 *
 * ThreadCleanup --
 *
 *      Destory per-thread sections on thread exit.
 *
 * Results:
 *      None.
 *
 * Side effects:
 *      Holds the global lock throughout to ensure another
 *      thread does not try to update this threads parameters.
 *
 *----------------------------------------------------------------------
 */

static void
ThreadCleanup(void *arg)
{
    Tcl_HashTable  *threadTable = arg;
    Tcl_HashEntry  *hPtr;
    Tcl_HashSearch  sectionSearch, paramSearch;
    Section        *threadSection;
    Param          *paramPtr;

    Ns_MutexLock(&lock);

    hPtr = Tcl_FirstHashEntry(threadTable, &sectionSearch);
    while (hPtr != NULL) {

        threadSection = (Section *) Tcl_GetHashValue(hPtr);

        hPtr =  Tcl_FirstHashEntry(&threadSection->params, &paramSearch);
        while (hPtr != NULL) {
            paramPtr = (Param *) Tcl_GetHashValue(hPtr);
            FreeParam(paramPtr);
            ns_free(paramPtr);
            hPtr = Tcl_NextHashEntry(&paramSearch);
        }
        Tcl_DeleteHashTable(&threadSection->params);

        if (threadSection->prevPtr != NULL) {
            threadSection->prevPtr->nextPtr = threadSection->nextPtr;
        }

        ns_free(threadSection);
        hPtr = Tcl_NextHashEntry(&sectionSearch);
    }
    Ns_MutexUnlock(&lock);

    Tcl_DeleteHashTable(threadTable);
    ns_free(threadTable);
}
