/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is AOLserver Code and related documentation
 * distributed by AOL.
 * 
 * The Initial Developer of the Original Code is America Online,
 * Inc. Portions created by AOL are Copyright (C) 1999 America Online,
 * Inc. All Rights Reserved.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU General Public License (the "GPL"), in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above, a recipient may use your
 * version of this file under either the License or the GPL.
 */

/*
 * nsconfigrw.h --
 *
 *      Definitions for prototype r/w configuration storage module.
 *
 * $Header$
 */

#ifndef NSCONFIGRW_H
#define NSCONFIGRW_H

#include "ns.h"


#ifdef NSCONFIGRW_EXPORTS
# undef NS_EXTERN
# define NS_EXTERN extern NS_EXPORT
#endif


NS_EXTERN void
Cfg_LibInit(void);

NS_EXTERN int
Cfg_GetString(CONST char *sect, CONST char *key, CONST char *def,
                 Tcl_DString *dsPtr);

NS_EXTERN int
Cfg_GetBool(CONST char *sect, CONST char *key, CONST int *def, int *value);

NS_EXTERN int
Cfg_GetInt(CONST char *sect, CONST char *key, CONST int *def, int *value);

NS_EXTERN int
Cfg_GetRange(CONST char *sect, CONST char *key, CONST int *def,
             CONST int min, CONST int max, int *value);

NS_EXTERN void
Cfg_SetString(CONST char *section, CONST char *key, char *value);

NS_EXTERN void
Cfg_SetInt(CONST char *section, CONST char *key, int value);

NS_EXTERN void
Cfg_SetBool(CONST char *section, CONST char *key, int value);


#endif /* NSCONFIGRW_H */
