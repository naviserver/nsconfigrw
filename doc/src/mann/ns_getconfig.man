[manpage_begin ns_getconfig n ALPHA]
[moddesc   {NaviServer Modules}]
[titledesc {Configuration parameters (read/write)}]


[description]
This is an experimental read/write version of the [cmd ns_config]
command. Configuration parameters are read from a central store and cached
per-thread. Parameters which are updated are pushed to the per-thread caches by
the updating thread. Reads should be relatively cheap and uncontended.



[section COMMANDS]
[list_begin definitions]

[call [cmd ns_getconfig] \
      [opt "[option -bool] | [option -int]" ] \
      [opt [option "-min [arg intmax]"] ] \
      [opt [option "-max [arg intmin]"] ] \
      [opt [arg section] ] \
	  [opt [arg key] ] \
	  [opt [arg default]] ]

Returns the value of the parameter [arg key] in [arg section]. If [arg key] is
not given then return a list of all keys in the [arg section]. If [arg section]
is not given then return a list of all known sections.


[call [cmd ns_setconfig] \
	  [arg section] \
	  [arg key] \
	  [arg value] ]

Update an existing configuration parameter, or if one does not already exist,
create one...

[list_end]



[section EXAMPLES]
FIXME




[see_also ns_config]
[keywords configuration parameter startup]
[manpage_end]
